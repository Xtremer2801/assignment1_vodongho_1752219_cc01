package net.codejava;

public class Utility {
	public static int Ground = 999;
	
	public static boolean isPrime(int num) {
		if (num < 2) {
			return false;
		}
		for (int i = 2; i < num / 2; i++) {
			if (num % i == 0){
				return false;
			}
		}
		return true;
	}
	
	public static boolean isSquare(int num) {
		int x = (int) Math.sqrt(num);
		return x*x == num;
	}
	
	public static boolean isAvalon(int num) {
		if (num == 999) {
			return true;
		}
		else return false;
	}
}