package net.codejava;

public class Warrior extends Fighter{
	Warrior(int BaseHp, int Wp) {
		super(BaseHp, Wp);
	}
	
	public double getCombatScore() {
		Utility utility = new Utility();
		double finalScore;
		
		if(getWp() == 1) {
			finalScore = getBaseHp();
		}
		else finalScore = getBaseHp() / 10.0;
		
		if (utility.isPrime(utility.Ground)) {
			finalScore = getBaseHp()*2;
		}
		
		return finalScore;
	}
}
