package net.codejava;

public class Knight extends Fighter{
	Knight(int BaseHp, int Wp){
		super(BaseHp, Wp);
	}
	
	public double getCombatScore() {
		Utility utility = new Utility();
		double finalScore;
		
		if(getWp() == 1) {
			finalScore = getBaseHp();
		}
		else finalScore = getBaseHp() / 10.0;
		
		if (utility.isSquare(utility.Ground)) {
			finalScore = getBaseHp()*2;
		}
		
		if (utility.isAvalon(utility.Ground)) {
			int n1 = 0, n2 = 1, n3 = 0;    
		    
			for(int i = 2; n3 < 2*getBaseHp(); i++) {    
				n3=n1+n2;      
				n1=n2;    
				n2=n3;    
			}
			
			finalScore = n3;
		}
		
		return finalScore;
	}
}
