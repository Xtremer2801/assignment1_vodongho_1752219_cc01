package net.codejava;

public class Fighter {
	public int mBaseHp;
	public int mWp;
	
	public double getCombatScore() {
		return 0;
	}
	
	Fighter(int BaseHp, int Wp) {
		mBaseHp = BaseHp;
		mWp = Wp;
	}
	
	public double getBaseHp() {
		return mBaseHp;
	}
	
	public void setBaseHp(int BaseHp) {
		mBaseHp = BaseHp;
	}
	
	public int getWp() {
		return mWp;
	}
	
	public void setWp(int Wp) {
		mWp = Wp;
	}
}